package com.addcel.PuntoWeb.util;

public interface Constantes {
    String ID_APPLICATION = "Punto Web";
    String ACTIVO = "T";

    //KEY PARAM PUNTO_WEB_CONFIG_PARAM
    String COMERCIO_REQUEST = "COMERCIO_REQUEST";
    String MARCA_REQUEST = "MARCA_REQUEST";
    String COMERCIO_FACILITADOR_REQUEST = "COMERCIO_FACILITADOR_REQUEST";
    String KEY_MERCHANT_SOLES = "KEY_MERCHANT_SOLES";

    String CODE_RESPUESTA_EXITO = "00";
}
